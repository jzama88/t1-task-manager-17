package com.t1.alieva.tm.api.model;

import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;

public interface ICommand {

    String getName();

    String getArgument();

    String getDescription();

    void execute() throws AbstractEntityNotFoundException, AbstractFieldException;
}
